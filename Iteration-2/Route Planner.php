<?php
        $locations=array();
        $locations2=array();
        $locations3=array();
        $animals=array();
		$uname="bn_wordpress";
        $pass="39441b972c";
        $servername="localhost:3306";
        $dbname="bitnami_wordpress";
        $db=new mysqli($servername,$uname,$pass,$dbname);
        $query =  $db->query("SELECT * FROM wp_crash WHERE LIGHT_CODN='Day'");
        while( $row = $query->fetch_assoc()){
            $longitude = $row['LONGITUDE'];                              
            $latitude = $row['LATITUDE'];
            /* Each row is added as a new array */
            $locations[]=array('lat'=>$latitude, 'lng'=>$longitude);
            
        }//echo $locations[2]['lat'];
        $query2 =  $db->query("SELECT * FROM wp_crash WHERE Light_Codn='Dusk/Dawn'");
        while( $row = $query2->fetch_assoc()){
            $longitude = $row['LONGITUDE'];                              
            $latitude = $row['LATITUDE'];
            /* Each row is added as a new array */
            $locations2[]=array('lat'=>$latitude, 'lng'=>$longitude);
            
        }//echo $locations2[2]['lat'];
        $query3 =  $db->query("SELECT * FROM wp_crash WHERE Light_Codn='Dark No street lights'");
        while( $row = $query3->fetch_assoc()){
            $longitude = $row['LONGITUDE'];                              
            $latitude = $row['LATITUDE'];
            /* Each row is added as a new array */
            $locations3[]=array('lat'=>$latitude, 'lng'=>$longitude);
            
        }//echo $locations3[0]['lat'];

        $query4 =  $db->query("SELECT * FROM wp_animal WHERE (NAME LIKE '%Koala%' or NAME LIKE '%Kangaroo%' or NAME LIKE '%Wallaby%') & (YEAR between '2005' and '2019') ");
        while( $rows = $query4->fetch_assoc()){
            $names = $rows['NAME'];
            $longitudes = $rows['LONGITUDE'];                              
                $latitudes = $rows['LATITUDE'];
                /* Each row is added as a new array */
                $animals[]=array('name'=>$names ,'lt'=>$latitudes, 'lg'=>$longitudes);
        }	//echo $animals[0]['name'];
?>  
<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Plan</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="linearicons.css">
			<link rel="stylesheet" href="font-awesome.min.css">
			<link rel="stylesheet" href="bootstrap.css">
			<link rel="stylesheet" href="magnific-popup.css">
			<link rel="stylesheet" href="jquery-ui.css">				
			<link rel="stylesheet" href="nice-select.css">							
			<link rel="stylesheet" href="animate.min.css">
			<link rel="stylesheet" href="owl.carousel.css">				
			<link rel="stylesheet" href="main.css">
        <style>
            .section-gap{
				padding :50px 0;
			}
			
        </style>
		</head>
		<body>	
			<header id="header">
				<div class="container main-menu"style="padding-top:0px;padding-bottom:0px;background:#212529">
					<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a><img src="logo.png" alt="" title="" /></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
                          <input type="button" onclick="location.href='https://www.aniway.ml'" style="background-color:transparent;border: none;color:#fff;cursor:pointer;width:75px;height:30px;" value="HOME">
                          <input type="button" style="background-color:transparent;border: none;color:#fff;cursor:pointer;width:150px;height:30px;" value="ROUTE PLANNER">
                          <input type="button" onclick="location.href='https://www.aniway.ml/find-help/'" style="background-color:transparent;border: none;color:#fff;cursor:pointer;width:75px;height:30px;" value="FIND HELP">
                        </ul>
				      </nav><!-- #nav-menu-container -->					      		  
					</div>
				</div>
			</header><!-- #header -->
			
			<!-- start banner Area -->
			<section class="banner-area relative" style="background-image: url(road.jpg);">
                <div class="overlay overlay-bg"></div>	
				<div class="container">
					<div class="row fullscreen align-items-center justify-content-between">
                        <div class="col-lg-6 col-md-6 banner-left">
							<h1 class="text-white">Live and let live</h1>
                            <h6 style="text-align: justify; text-justify: inter-word;"class="text-white">Our simple route planner allows you to identify high risk areas for collision and the animals that you
                            may encounter in these areas. All you need to do to get started is to enter your trip details!</h6>
						</div>
						<div class="col-lg-4 col-md-6 banner-right">
							 <div class="tab-content" id="myTabContent">
							  <div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="flight-tab">
								<div class="form-style-5">
                                    <div id="floating-panel">
                                        <form id="myform">
                                            <fieldset>
                                                <legend><span class="number" style="background: #d65050;color: #fff;height: 30px;width: 30px;display: inline-block;font-size: 0.8em;margin-right: 4px;line-height: 30px;text-align: center;text-shadow: 0 1px 0 rgba(255,255,255,0.2);border-radius: 15px 15px 15px 0px;">1</span> Travel Info</legend>
                                                <label>Source: </label>
                                                <input type="text" id="start">
                                                <label>Destination: </label>
                                                <input type="text" id="end">
                                                 <label for="time">Departure Time:</label>
                                                <select id="light_codn">
                                                <option value="" selected>Select Time</option>
                                                <option value="Day">Between 6am and 6pm</option>
                                                <option value="DawnDusk">5am to 6am and 6pm to 7pm</option>
                                                <option value="Night">After 7pm</option>
                                                </select> 
                                            </fieldset>
                                            <fieldset>
                                                <legend><span class="number" style="background: #d65050;color: #fff;height: 30px;width: 30px;display: inline-block;font-size: 0.8em;margin-right: 4px;line-height: 30px;text-align: center;text-shadow: 0 1px 0 rgba(255,255,255,0.2);border-radius: 15px 15px 15px 0px;">2</span> Show animals</legend>
												<input type="radio" id="animal_data" name="animal_data" value="" checked>None
												<input type="radio" id="animal_data" name="animal_data" value="Animals">All animals
												<input type="radio" id="animal_data" name="animal_data" value="Kangaroo">Kangaroo<br>
												<input type="radio" id="animal_data" name="animal_data" value="Wallaby">Wallaby
												<input type="radio" id="animal_data" name="animal_data" value="Koala">Koala<br>
                                          </fieldset>
                                        </form>
                                        <div id="learn">
                                        <a href="#a2"> <input style="position: relative;display: block;padding: 19px 39px 18px 39px;color: #FFF;margin: 0 auto;background: #d65050;font-size: 18px;text-align: center;font-style: normal;width: 100%;border: 1px solid #d65050;border-width: 1px 1px 3px;margin-bottom: 10px; cursor:pointer;"id="submit" type="submit" value="Go" onclick="validate();" /></a>
                                            <script>
                                                function validate(){
                                                    {
                                                      var st= document.getElementById("start").value;
                                                      var en= document.getElementById("end").value;
                                                      if(st == null || st == "" || en == null || en == "")
                                                      {
                                                        alert("Fill in all the details");
														window.location.href=window.location.href;
                                                        return false;
                                                      }
                                                    else{
                                                        initMap();
                                                    }
                                                    }
                                                }
                                            </script>
                                    </div>
                                </div>
				            </div>
							</div>
						</div>           
				</div>
                </div>
                </div>
			</section>
			<!-- End banner Area -->
			<!-- Start map Area -->
			<section id="a2" class="price-area section-gap" style="background-image: url(price-bg.png);">
                <p align="center"><strong> The map will display high risk areas as a heat map and the location of wildlife is shown by markers. From there, you may alter the route by selecting and dragging as necessary. Want to know popular destinations across Victoria. Click to find more   .</strong>
                <a href="#a3"><input type="button" style="background-color:transparent;border: none;color:#d65050;cursor:pointer;width:75px;height:30px;" value="CLICK ME!"></a></p>
                <div class="banner-right">
                <div id="panel" style="position:absolute;width:300px;height:500px;overflow-y:auto;float:right;right:150px;padding-left:50px;"></div>  
                </div>   
                <div id="output" style="float: left;position: absolute; padding-top:50px;padding-left:150px; z-index: -1;"></div>
                <div class="col-lg-6 col-md-6 banner-left">
                <div id="googleMap" style="height: 500px;width: 900px">
                    
                    </div> 
                </div>    
				<script>
					function getRadioVal(form, name) {
						var val;
						// get list of radio buttons with specified name
						var radios = form.elements[name];
						
						// loop through list of radio buttons
						for (var i=0, len=radios.length; i<len; i++) {
							if ( radios[i].checked ) { // radio checked?
								val = radios[i].value; // if so, hold its value in val
								break; // and break out of for loop
							}
						}
						return val; // return value of checked radio or undefined if none checked
					}
				</script>
                <script>
                    
                function initMap() {
                    
                var directionsDisplay = "";
                var directionsService = "";
                var infoWindow = "";
                var map = new google.maps.Map(document.getElementById('googleMap'), {
                    zoom: 6,
                    center: {lat: -37.889530, lng: 145.043590},
                     mapTypeId: 'satellite'
                });
                    
                var ar = <?php echo json_encode($animals);?>;
                var animalMarker,x;
                var animal_marker = [];
                var image_animal = 'icon_animal.png';
                var image_kan = 'icon_kan.png';
                var image_koa = 'icon_koa.png';
                var image_wall = 'icon_wally.png';
                
                var heatmap,heatmapData=[];	
                var menu = document.getElementById("light_codn");
                var menu2 = getRadioVal( document.getElementById('myform'), 'animal_data' );
                var locations = [
                    <?php for($i=0;$i<sizeof($locations);$i++){ $j=$i+1;?>
                    [
                        <?php echo $locations[$i]['lat'];?>,
                        <?php echo $locations[$i]['lng'];?>,
                        0
                    ]<?php if($j!=sizeof($locations))echo ","; }?>
                ];
                var locations2 = [
                    <?php for($i=0;$i<sizeof($locations2);$i++){ $j=$i+1;?>
                    [
                        <?php echo $locations2[$i]['lat'];?>,
                        <?php echo $locations2[$i]['lng'];?>,
                        0
                    ]<?php if($j!=sizeof($locations2))echo ","; }?>
                ];
                var locations3 = [
                    <?php for($i=0;$i<sizeof($locations3);$i++){ $j=$i+1;?>
                    [
                        <?php echo $locations3[$i]['lat'];?>,
                        <?php echo $locations3[$i]['lng'];?>,
                        0
                    ]<?php if($j!=sizeof($locations3))echo ","; }?>
                ];
                    
                $("#submit").click(function(){
                if (menu.value == 'Day') {
                //alert('1');
                heatmap = new google.maps.visualization.HeatmapLayer({
                          data: getPoints(),
                          map: map,
                          radius: 20
                        });
                function getPoints() {
                for (var i = 0, l = locations.length; i < l; i++) {
                      heatmapData.push(new google.maps.LatLng(locations[i][0], locations[i][1]));
                  }
                    return heatmapData;
                  }
                
                }
                    
                if (menu.value == 'DawnDusk') { 
                //alert('2');
                heatmap = new google.maps.visualization.HeatmapLayer({
                          data: getPoints(),
                          map: map,
                          radius: 20
                        });
                function getPoints() {
                for (var i = 0, l = locations2.length; i < l; i++) {
                      heatmapData.push(new google.maps.LatLng(locations2[i][0], locations2[i][1]));
                  }
                    return heatmapData;
                  }
                
                }
                    
                if (menu.value == 'Night') { 
                //alert('3');
                heatmap = new google.maps.visualization.HeatmapLayer({
                          data: getPoints(),
                          map: map,
                          radius: 20
                        });
                function getPoints() {
                for (var i = 0, l = locations3.length; i < l; i++) {
                      heatmapData.push(new google.maps.LatLng(locations3[i][0], locations3[i][1]));
                  }
                    return heatmapData;
                  }
                
                }
                });
                    
                var arraa = [];
                var arral = [];
                var ltarr = [];
                var arrall =[];
                var lgarr = [];

                $("#submit").click(function(){

                if (menu2 == 'Animals') {
                    //alert('zoo');
                      for (var i = 0; i < ar.length; i++){
                      if (ar[i].name){
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lt);
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lg);
                  }
                  }
                  for(var i = 0; i < arraa.length; i += 2) {  // take every second element
                    arral.push(arraa[i]);
                  }
                  for(var i = 1; i < arraa.length; i += 2) {  // take every second element
                    arrall.push(arraa[i]);
                  }
                  ltarr = arral.map(Number);
                  lgarr = arrall.map(Number);
                  var markers = [];
                  for (var i = 0; i < ltarr.length; i++) {

                      var marker = new google.maps.Marker({
                        position: {lat: ltarr[i], lng: lgarr[i]},
                        map: map,
                        icon: image_animal
                    });
                    markers.push(marker)
                  }
                  var clusterStyles = [
                    {textColor: 'black',
                    url: 'a1.png',
                    height: 50,
                    width: 50},
                    {textColor: 'blue',
                    url: 'a2.png',
                    height: 50,
                    width: 50},
                    {textColor: 'black',
                    url: 'a3.png',
                    height: 50,
                    width: 50},
                    ];

                  var mcOptions = {
                        styles: clusterStyles,
                    };
                  var markerCluster = new MarkerClusterer(map, markers, mcOptions);  
                }
                    
                if (menu2 == 'Kangaroo') {
                      for (var i = 0; i < ar.length; i++){
                      if ((ar[i].name).match(/Kangaroo/)){
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lt);
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lg);
                  }
                  }
                  for(var i = 0; i < arraa.length; i += 2) {  // take every second element
                    arral.push(arraa[i]);
                  }
                  for(var i = 1; i < arraa.length; i += 2) {  // take every second element
                    arrall.push(arraa[i]);
                  }
                  ltarr = arral.map(Number);
                  lgarr = arrall.map(Number);
                  var markers = [];
                  for (var i = 0; i < ltarr.length; i++) {

                      var marker = new google.maps.Marker({
                        position: {lat: ltarr[i], lng: lgarr[i]},
                        map: map,
                        icon: image_kan
                    });
                    markers.push(marker)
                  }
                  var clusterStyles = [
                    {textColor: 'black',
                    url: 'm4.png',
                    height: 40,
                    width: 40},
                    {textColor: 'black',
                    url: 'm4.png',
                    height: 40,
                    width: 40},
                    {textColor: 'black',
                    url: 'm4.png',
                    height: 40,
                    width: 40},
                    ];

                  var mcOptions = {
                        styles: clusterStyles,
                    };
                  var markerCluster = new MarkerClusterer(map, markers, mcOptions);  
                }
                    
                 if (menu2 == 'Wallaby') {
                      for (var i = 0; i < ar.length; i++){
                      if ((ar[i].name).match(/Wallaby/)){
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lt);
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lg);
                  }
                  }
                  for(var i = 0; i < arraa.length; i += 2) {  // take every second element
                    arral.push(arraa[i]);
                  }
                  for(var i = 1; i < arraa.length; i += 2) {  // take every second element
                    arrall.push(arraa[i]);
                  }
                  ltarr = arral.map(Number);
                  lgarr = arrall.map(Number);
                  var markers = [];
                  for (var i = 0; i < ltarr.length; i++) {

                      var marker = new google.maps.Marker({
                        position: {lat: ltarr[i], lng: lgarr[i]},
                        map: map,
                        icon: image_wall
                    });
                    markers.push(marker)
                  }
                  var clusterStyles = [
                    {textColor: 'black',
                    url: 'w4.png',
                    height: 40,
                    width: 40},
                    {textColor: 'black',
                    url: 'w4.png',
                    height: 40,
                    width: 40},
                    {textColor: 'black',
                    url: 'w4.png',
                    height: 40,
                    width: 40},
                    ];

                  var mcOptions = {
                        styles: clusterStyles,
                    };
                  var markerCluster = new MarkerClusterer(map, markers, mcOptions);  
                }
                 if (menu2 == 'Koala') {
                      for (var i = 0; i < ar.length; i++){
                      if ((ar[i].name).match(/Koala/)){
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lt);
                      arraa.push(document.getElementById('output').innerHTML=ar[i].lg);
                  }
                  }
                  for(var i = 0; i < arraa.length; i += 2) {  // take every second element
                    arral.push(arraa[i]);
                  }
                  for(var i = 1; i < arraa.length; i += 2) {  // take every second element
                    arrall.push(arraa[i]);
                  }
                  ltarr = arral.map(Number);
                  lgarr = arrall.map(Number);
                  var markers = [];
                  for (var i = 0; i < ltarr.length; i++) {

                      var marker = new google.maps.Marker({
                        position: {lat: ltarr[i], lng: lgarr[i]},
                        map: map,
                        icon: image_koa
                    });
                    markers.push(marker)
                  }
                  var clusterStyles = [
                    {textColor: 'black',
                    url: 'k4.png',
                    height: 40,
                    width: 40},
                    {textColor: 'black',
                    url: 'k4.png',
                    height: 40,
                    width: 40},
                    {textColor: 'black',
                    url: 'k4.png',
                    height: 40,
                    width: 40},
                    ];

                  var mcOptions = {
                        styles: clusterStyles,
                    };

                  var markerCluster = new MarkerClusterer(map, markers, mcOptions);  
                }
                });
                
                var op = {
                          componentRestrictions: {country: "au"}
                         };
                var autocomplete = new google.maps.places.Autocomplete((document.getElementById('start')),op);
                var autocompletes = new google.maps.places.Autocomplete((document.getElementById('end')),op);
	
                directionsService = new google.maps.DirectionsService;
                directionsDisplay = new google.maps.DirectionsRenderer({
                    draggable: true,
                    map: map,
                    panel: document.getElementById('right-panel'),
                    provideRouteAlternatives:true    
                });
                infoWindow = new google.maps.InfoWindow;

                directionsDisplay.addListener('directions_changed', function() {
                computeTotalDistance(directionsDisplay.getDirections());
                });

                directionsDisplay.setMap(map);
                $('#panel').empty();
                directionsDisplay.setPanel(document.getElementById('panel'));
                displayRoute(document.getElementById( 'start').value, document.getElementById( 'end').value, directionsService,
                directionsDisplay);
                }

                function displayRoute(origin, destination, service, display) {
                service.route({
                origin: origin,
                destination: destination,
                travelMode: 'DRIVING',
                avoidTolls: true
                }, function(response, status) {
                if (status === 'OK') {
                display.setDirections(response);
                } 
                });
                }

                  function computeTotalDistance(result) {
                    var total = 0;
                    var myroute = result.routes[0];
                    for (var i = 0; i < myroute.legs.length; i++) {
                      total += myroute.legs[i].distance.value;
                    }
                    total = total / 1000;
                    document.getElementById('total').innerHTML = total + ' km';
                  }
                google.maps.event.addDomListener(window, 'load', initialize);
                
                </script>
                <p> </p>
            </section>    
            <!--End map area-->	

            <!-- Start destination Area -->
			<section id="a3" class="recent-blog-area section-gap">
				<div class="container">
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-60 col-lg-9">
							<div class="title text-center">
								<h1 class="mb-10">Popular Destinations</h1>
								<p>These destinations have always been a favorite to Road trip freaks.</p>
							</div>
						</div>
					</div>							
					<div class="row">
						<div class="active-recent-blog-carusel">
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="12apo.jpg" alt="">
								</div>
								<div class="details">
									
									<a href="#"><h4 class="title">Great Ocean Road</h4></a>
									<p style="text-align: justify; text-justify: inter-word;">
										See the towering 12 Apostles, get up close to native wildlife, and take in iconic surf breaks, pristine rainforest and misty waterfalls along the spectacular Great Ocean Road. 
									</p>
								</div>	
							</div>
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="pp.jpg" alt="">
								</div>
								<div class="details">
									
									<a href="#"><h4 class="title">Philip Island</h4></a>
									<p style="text-align: justify; text-justify: inter-word;">
										Meet the lovely penguins. Experience that electrifying moment as you spot the first little penguins emerging from the sea and waddling along a Phillip Island beach at dusk.
									</p>
									
								</div>	
							</div>
							<div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="mt.jpg" alt="">
								</div>
								<div class="details">
									<a href="#"><h4 class="title">Mt Buller</h4></a>
									<p style="text-align: justify; text-justify: inter-word;">
										An easy three-hour drive from Melbourne, Mount Buller is the most accessible major snow resort in Australia and a premier ski destination for snow enthusiasts from across the nation and around the world.
									</p>
									
								</div>	
							</div>	
									
                            <div class="single-recent-blog-post item">
								<div class="thumb">
									<img class="img-fluid" src="wil.jpg" alt="">
								</div>
								<div class="details">
									<a href="#"><h4 class="title">Wilsons Promotory</h4></a>
									<p style="text-align: justify; text-justify: inter-word;">
										Be awed by Victoria's largest coastal wilderness area in Wilsons Promontory National Park. Affectionately known as 'The Prom', it is one of the state's best loved parks � and with good reason.
									</p>
									
								</div>	
							</div>	

						</div>
					</div>
				</div>	
			</section>
			<!-- End recent-blog Area -->	
            
			
			<!-- start footer Area -->	
				<footer style="position: absolute;bottom: 0;width: 100%;height: 50px;background:black;">Copyright@2019 ICAI</footer>
			<!-- End footer Area -->		
            <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
            </script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js">
            </script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js">
            </script>
            <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBthqk-LcIJvGPTYuOzw4Zxpk7ZadaralQ&libraries=places,visualization&callback=initMap">
            </script>
            <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
	       </script>
            <script src="jquery-2.2.4.min.js"></script>
            <script src="jquery-ui.js"></script>					
  			<script src="easing.min.js"></script>			
			<script src="hoverIntent.js"></script>
			<script src="superfish.min.js"></script>	
			<script src="jquery.ajaxchimp.min.js"></script>
			<script src="jquery.magnific-popup.min.js"></script>						
			<script src="jquery.nice-select.min.js"></script>					
			<script src="owl.carousel.min.js"></script>							
			<script src="mail-script.js"></script>	
			<script src="main.js"></script>	
		</body>
	</html>