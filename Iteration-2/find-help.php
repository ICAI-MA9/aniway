
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="https://www.aniway.ml/xmlrpc.php">

<title>Find Help &#8211; Aniway</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com'/>
<link rel='dns-prefetch' href='//s.w.org'/>
<link rel="alternate" type="application/rss+xml" title="Aniway &raquo; Feed" href="https://www.aniway.ml/feed/"/>
<link rel="alternate" type="application/rss+xml" title="Aniway &raquo; Comments Feed" href="https://www.aniway.ml/comments/feed/"/>
		<script type="text/javascript">window._wpemojiSettings={"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.aniway.ml\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.1.1"}};!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);</script>
		<style type="text/css">img.wp-smiley,img.emoji{display:inline!important;border:none!important;box-shadow:none!important;height:1em!important;width:1em!important;margin:0 .07em!important;vertical-align:-.1em!important;background:none!important;padding:0!important}</style>
	<link rel='stylesheet' id='sydney-bootstrap-css' href='https://www.aniway.ml/wp-content/themes/sydney/css/bootstrap/bootstrap.min.css?ver=1' type='text/css' media='all'/>
<link rel='stylesheet' id='wp-block-library-css' href='https://www.aniway.ml/wp-includes/css/dist/block-library/style.min.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='wp-travel-style-front-end-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/wp-travel-front-end.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='wp-travel-style-popup-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/magnific-popup.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='dashicons-css' href='https://www.aniway.ml/wp-includes/css/dashicons.min.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='easy-responsive-tabs-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/easy-responsive-tabs.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='Inconsolata-css' href='https://fonts.googleapis.com/css?family=Inconsolata&#038;ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='wp-travel-itineraries-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/wp-travel-itineraries.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='font-awesome-css-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/lib/font-awesome/css/fontawesome-all.min.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='wp-travel-fa-css-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/lib/font-awesome/css/wp-travel-fa-icons.min.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='wp-travel-user-css-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/wp-travel-user-styles.min.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery-datepicker-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/css/lib/datepicker/datepicker.css?ver=1.9.5' type='text/css' media='all'/>
<link rel='stylesheet' id='wp-travel-coupons-frontend-css-css' href='https://www.aniway.ml/wp-content/plugins/wp-travel/inc/coupon/assets/css/wp-travel-coupons-frontend.min.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='contact-form-7-css' href='https://www.aniway.ml/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.1' type='text/css' media='all'/>
<link rel='stylesheet' id='hfe-style-css' href='https://www.aniway.ml/wp-content/plugins/header-footer-elementor/assets/css/header-footer-elementor.css?ver=1.1.2' type='text/css' media='all'/>
<link rel='stylesheet' id='elementor-icons-css' href='https://www.aniway.ml/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=4.3.0' type='text/css' media='all'/>
<link rel='stylesheet' id='font-awesome-css' href='https://www.aniway.ml/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all'/>
<link rel='stylesheet' id='elementor-animations-css' href='https://www.aniway.ml/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=2.5.14' type='text/css' media='all'/>
<link rel='stylesheet' id='elementor-frontend-css' href='https://www.aniway.ml/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.5.14' type='text/css' media='all'/>
<link rel='stylesheet' id='she-header-style-css' href='https://www.aniway.ml/wp-content/plugins/sticky-header-effects-for-elementor/assets/css/she-header-style.css?ver=1.3.1' type='text/css' media='all'/>
<link rel='stylesheet' id='elementor-global-css' href='https://www.aniway.ml/wp-content/uploads/elementor/css/global.css?ver=1556346095' type='text/css' media='all'/>
<link rel='stylesheet' id='elementor-post-1281-css' href='https://www.aniway.ml/wp-content/uploads/elementor/css/post-1281.css?ver=1556437148' type='text/css' media='all'/>
<link rel='stylesheet' id='elementor-post-159-css' href='https://www.aniway.ml/wp-content/uploads/elementor/css/post-159.css?ver=1556325102' type='text/css' media='all'/>
<link rel='stylesheet' id='sydney-fonts-css' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro%3A400%2C400italic%2C600%7CRaleway%3A400%2C500%2C600' type='text/css' media='all'/>
<link rel='stylesheet' id='sydney-style-css' href='https://www.aniway.ml/wp-content/themes/sydney/style.css?ver=20180710' type='text/css' media='all'/>
<style id='sydney-style-inline-css' type='text/css'>.site-header{background-color:rgba(0,0,0,.9)}.site-title{font-size:32px}.site-description{font-size:12px}#mainnav ul li a{font-size:12px}h1{font-size:52px}h2{font-size:42px}h3{font-size:32px}h4{font-size:25px}h5{font-size:20px}h6{font-size:18px}body{font-size:16px}.single .hentry .title-post{font-size:36px}.header-image{background-size:cover}.header-image{height:1000px}.site-header.float-header{background-color:rgba(0,0,0,.9)}@media only screen and (max-width:1024px){.site-header{background-color:#000}}.site-title a,.site-title a:hover{color:#fff}.site-description{color:#fff}#mainnav ul li a,#mainnav ul li::before{color:#fff}#mainnav .sub-menu li a{color:#fff}#mainnav .sub-menu li a{background:#1c1c1c}.text-slider .maintitle,.text-slider .subtitle{color:#fff}body{color:#47425d}#secondary{background-color:#fff}#secondary,#secondary a,#secondary .widget-title{color:#767676}.footer-widgets{background-color:#252525}.btn-menu{color:#fff}#mainnav ul li a:hover{color:#d65050}.site-footer{background-color:#1c1c1c}.site-footer,.site-footer a{color:#666}.overlay{background-color:#000}.page-wrap{padding-top:83px}.page-wrap{padding-bottom:100px}.slide-inner{display:none}.slide-inner.text-slider-stopped{display:block}@media only screen and (max-width:1025px){.mobile-slide{display:block}.slide-item{background-image:none!important}.header-slider{}.slide-item{height:auto!important}.slide-inner{min-height:initial}}@media only screen and (max-width:780px){h1{font-size:32px}h2{font-size:28px}h3{font-size:22px}h4{font-size:18px}h5{font-size:16px}h6{font-size:14px}}</style>
<link rel='stylesheet' id='sydney-font-awesome-css' href='https://www.aniway.ml/wp-content/themes/sydney/fonts/font-awesome.min.css?ver=5.1.1' type='text/css' media='all'/>
<!--[if lte IE 9]>
<link rel='stylesheet' id='sydney-ie9-css'  href='https://www.aniway.ml/wp-content/themes/sydney/css/ie9.css?ver=5.1.1' type='text/css' media='all' />
<![endif]-->
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/js/booking.js?ver=1.9.5'></script>
<script type='text/javascript'>//<![CDATA[
var wpgmza_google_api_status={"message":"Enqueued","code":"ENQUEUED"};
//]]></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/wp-google-maps/wpgmza_data.js?ver=5.1.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/sticky-header-effects-for-elementor/assets/js/she-header.js?ver=1.3.1'></script>
<link rel='https://api.w.org/' href='https://www.aniway.ml/wp-json/'/>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.aniway.ml/xmlrpc.php?rsd"/>
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.aniway.ml/wp-includes/wlwmanifest.xml"/> 
<meta name="generator" content="WordPress 5.1.1"/>
<link rel="canonical" href="https://www.aniway.ml/find-help/"/>
<link rel='shortlink' href='https://www.aniway.ml/?p=1281'/>
<link rel="alternate" type="application/json+oembed" href="https://www.aniway.ml/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.aniway.ml%2Ffind-help%2F"/>
<link rel="alternate" type="text/xml+oembed" href="https://www.aniway.ml/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.aniway.ml%2Ffind-help%2F&#038;format=xml"/>
<style id="mystickymenu" type="text/css">#mysticky-nav{width:100%;position:static}#mysticky-nav.wrapfixed{position:fixed;left:0;margin-top:0;z-index:99990;-webkit-transition:.3s;-moz-transition:.3s;-o-transition:.3s;transition:.3s;-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";filter: alpha(opacity=90);opacity:.9;background-color:#f7f5e7}#mysticky-nav .myfixed{margin:0 auto;float:none;border:0;background:none;max-width:100%}</style>		<style type="text/css">.recentcomments a{display:inline!important;padding:0!important;margin:0!important}</style>
		<style type="text/css" id="custom-background-css">body.custom-background{background-color:#fef7ed}</style>
	<link rel="icon" href="https://www.aniway.ml/wp-content/uploads/2019/04/websiteicon1-150x150.jpg" sizes="32x32"/>
<link rel="icon" href="https://www.aniway.ml/wp-content/uploads/2019/04/websiteicon1-300x300.jpg" sizes="192x192"/>
<link rel="apple-touch-icon-precomposed" href="https://www.aniway.ml/wp-content/uploads/2019/04/websiteicon1-300x300.jpg"/>
<meta name="msapplication-TileImage" content="https://www.aniway.ml/wp-content/uploads/2019/04/websiteicon1-300x300.jpg"/>
</head>

<body class="page-template page-template-page-templates page-template-page_front-page page-template-page-templatespage_front-page-php page page-id-1281 custom-background ehf-footer ehf-template-sydney ehf-stylesheet-sydney group-blog elementor-default elementor-page elementor-page-1281">

	<div class="preloader">
	    <div class="spinner">
	        <div class="pre-bounce1"></div>
	        <div class="pre-bounce2"></div>
	    </div>
	</div>
	
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

		
	<div class="header-clone"></div>

	
	<header id="masthead" class="site-header" role="banner">
		<div class="header-wrap">
            <div class="container">
                <div class="row">
				<div class="col-md-4 col-sm-8 col-xs-12">
		        					<a href="https://www.aniway.ml/" title="Aniway"><img class="site-logo" src="http://35.225.91.187/wp-content/uploads/2019/04/LogoMakr_6zXcwO.png" alt="Aniway"/></a>
		        				</div>
				<div class="col-md-8 col-sm-4 col-xs-12">
					<div class="btn-menu"></div>
					<nav id="mainnav" class="mainnav" role="navigation">
						<div class="menu-main-menu-container"><ul id="menu-main-menu" class="menu"><li id="menu-item-529" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-529"><a href="http://aniway.ml">HOME</a></li>
<li id="menu-item-739" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-739"><a href="http://aniway.ml/planner_v2.php">ROUTE PLANNER</a></li>
<li id="menu-item-1440" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1281 current_page_item menu-item-1440"><a href="https://www.aniway.ml/find-help/" aria-current="page">FIND HELP</a></li>
</ul></div>					</nav><!-- #site-navigation -->
				</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	
	<div class="sydney-hero-area">
				<div class="header-image">
			<div class="overlay"></div>			<img class="header-inner" src="https://35.225.91.187/wp-content/uploads/2019/04/cropped-newbackground.png" width="1920" alt="Aniway" title="Aniway">
		</div>
		
			</div>

	
	<div id="content" class="page-wrap">
		<div class="container content-wrapper">
			<div class="row">	
	<div id="primary" class="fp-content-area">
		<main id="main" class="site-main" role="main">

			<div class="entry-content">
											<div data-elementor-type="post" data-elementor-id="1281" class="elementor elementor-1281" data-elementor-settings="[]">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
							<section class="elementor-element elementor-element-e022dbb elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="e022dbb" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-17d5329 elementor-column elementor-col-100 elementor-top-column" data-id="17d5329" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-5d10dd2 elementor-widget elementor-widget-spacer" data-id="5d10dd2" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-f9face1 elementor-widget elementor-widget-heading" data-id="f9face1" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">WHAT CAN YOU DO...?</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ebd8440 elementor-widget elementor-widget-text-editor" data-id="ebd8440" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Don&#8217;t know what to do when come across a wild animal on the road? </p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-97645f3 elementor-widget elementor-widget-text-editor" data-id="97645f3" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p><span style="color: #ffffff; font-family: Arial, sans-serif; font-size: 18px; text-align: center;">Let us help you, you can find some useful tips here and find the nearest veterinary clinic!</span></p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-98b3cc9 elementor-widget elementor-widget-spacer" data-id="98b3cc9" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-c3ffc10 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="c3ffc10" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-ad9b953 elementor-column elementor-col-100 elementor-top-column" data-id="ad9b953" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-ab18718 elementor-widget elementor-widget-spacer" data-id="ab18718" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-270d525 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="270d525" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-b2556ad elementor-column elementor-col-100 elementor-top-column" data-id="b2556ad" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-be6b111 elementor-widget elementor-widget-text-editor" data-id="be6b111" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Hover the photos to see the tips !</p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-32489d4 elementor-align-center elementor-widget elementor-widget-button" data-id="32489d4" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#Tips" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i class="fa fa-chevron-down" aria-hidden="true"></i>
			</span>
						<span class="elementor-button-text"></span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-ef1f6c5 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="ef1f6c5" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-07e78b9 elementor-column elementor-col-100 elementor-top-column" data-id="07e78b9" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-eeee020 elementor-widget elementor-widget-menu-anchor" data-id="eeee020" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="Tips" class="elementor-menu-anchor"></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-e07ac04 elementor-section-full_width elementor-section-content-middle elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="e07ac04" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-baa3f8f elementor-column elementor-col-33 elementor-top-column" data-id="baa3f8f" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-8c1f3ba elementor-widget elementor-widget-image" data-id="8c1f3ba" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="1024" height="683" src="https://www.aniway.ml/wp-content/uploads/2019/04/透明-1024x683.jpg" class="attachment-large size-large" alt="" srcset="https://www.aniway.ml/wp-content/uploads/2019/04/透明-1024x683.jpg 1024w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-300x200.jpg 300w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-768x512.jpg 768w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-830x553.jpg 830w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-230x153.jpg 230w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-350x233.jpg 350w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-480x320.jpg 480w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-272x182.jpg 272w" sizes="(max-width: 1024px) 100vw, 1024px"/>											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-e45ae5a elementor-column elementor-col-33 elementor-top-column" data-id="e45ae5a" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-a1f2e19 elementor-widget elementor-widget-image" data-id="a1f2e19" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="1024" height="683" src="https://www.aniway.ml/wp-content/uploads/2019/04/透明-1024x683.jpg" class="attachment-large size-large" alt="" srcset="https://www.aniway.ml/wp-content/uploads/2019/04/透明-1024x683.jpg 1024w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-300x200.jpg 300w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-768x512.jpg 768w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-830x553.jpg 830w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-230x153.jpg 230w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-350x233.jpg 350w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-480x320.jpg 480w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-272x182.jpg 272w" sizes="(max-width: 1024px) 100vw, 1024px"/>											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-67783e4 elementor-column elementor-col-33 elementor-top-column" data-id="67783e4" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-a9ea0ad elementor-widget elementor-widget-image" data-id="a9ea0ad" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="1024" height="683" src="https://www.aniway.ml/wp-content/uploads/2019/04/透明-1024x683.jpg" class="attachment-large size-large" alt="" srcset="https://www.aniway.ml/wp-content/uploads/2019/04/透明-1024x683.jpg 1024w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-300x200.jpg 300w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-768x512.jpg 768w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-830x553.jpg 830w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-230x153.jpg 230w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-350x233.jpg 350w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-480x320.jpg 480w, https://www.aniway.ml/wp-content/uploads/2019/04/透明-272x182.jpg 272w" sizes="(max-width: 1024px) 100vw, 1024px"/>											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-6340231 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="6340231" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-3b8c3b2 elementor-column elementor-col-33 elementor-top-column" data-id="3b8c3b2" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-982475d elementor-widget elementor-widget-heading" data-id="982475d" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Before you’re involved in a collision with native wildlife</h2>		</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-3af3485 elementor-column elementor-col-33 elementor-top-column" data-id="3af3485" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-ec685c3 elementor-widget elementor-widget-heading" data-id="ec685c3" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">If a collision with native wildlife seems imminent</h2>		</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-d77d7fa elementor-column elementor-col-33 elementor-top-column" data-id="d77d7fa" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-3e0b8e5 elementor-widget elementor-widget-heading" data-id="3e0b8e5" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">After you’re involved in a collision with native wildlife​</h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-26ff1e2 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="26ff1e2" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-b495aae elementor-column elementor-col-100 elementor-top-column" data-id="b495aae" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-feec9ad elementor-widget elementor-widget-text-editor" data-id="feec9ad" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Click Here to Explore </p></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-3e9b936 elementor-align-center elementor-widget__width-inherit elementor-widget elementor-widget-button" data-id="3e9b936" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#Findvet" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i class="fa fa-chevron-down" aria-hidden="true"></i>
			</span>
						<span class="elementor-button-text"></span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-b4a68b8 elementor-widget elementor-widget-heading" data-id="b4a68b8" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Your Nearest Veterinary Clinic!</h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-e6aad4c elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="e6aad4c" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-332da9f elementor-column elementor-col-100 elementor-top-column" data-id="332da9f" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-d9fefa1 elementor-widget elementor-widget-menu-anchor" data-id="d9fefa1" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="Findvet" class="elementor-menu-anchor"></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-ea58526 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="ea58526" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-26d9b88 elementor-column elementor-col-100 elementor-top-column" data-id="26d9b88" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-e143bb1 elementor-widget elementor-widget-html" data-id="e143bb1" data-element_type="widget" data-widget_type="html.default">
				<div class="elementor-widget-container">
			<html>
<head>
    <style type="text/css">.form-style-5{width:250px;height:250px;padding:10px 20px;background:#f4f7f8;margin:10px auto;padding:20px;background:#f4f7f8;border-radius:8px;float:left;font-family:Georgia,"Times New Roman",Times,serif}.form-style-5 fieldset{border:none}.form-style-5 legend{font-size:1.4em;margin-bottom:10px}.form-style-5 label{display:block;margin-bottom:8px}.form-style-5 input[type="text"],.form-style-5 input[type="number"],.form-style-5 input[type="search"],.form-style-5 textarea,.form-style-5 select{font-family:Georgia,"Times New Roman",Times,serif;background:rgba(255,255,255,.1);border:none;border-radius:4px;font-size:15px;margin:0;outline:0;padding:10px;width:100%;box-sizing:border-box;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;background-color:#e8eeef;color:#8a97a0;-webkit-box-shadow:0 1px 0 rgba(0,0,0,.03) inset;box-shadow:0 1px 0 rgba(0,0,0,.03) inset;margin-bottom:30px}.form-style-5 input[type="text"]:focus,.form-style-5 input[type="number"]:focus,.form-style-5 input[type="search"]:focus,.form-style-5 textarea:focus,.form-style-5 select:focus{background:#d2d9dd}.form-style-5 select{-webkit-appearance:menulist-button;height:35px}.form-style-5 .number{background:#d65050;color:#fff;height:30px;width:30px;display:inline-block;font-size:.8em;margin-right:4px;line-height:30px;text-align:center;text-shadow:0 1px 0 rgba(255,255,255,.2);border-radius:15px 15px 15px 0}.form-style-5 input[type="submit"],.form-style-5 input[type="button"]{position:relative;display:block;padding:19px 39px 18px 39px;color:#fff;margin:0 auto;background:#d65050;font-size:18px;text-align:center;font-style:normal;width:150px;border:1px solid #d65050;border-width:1px 1px 3px;margin-bottom:10px}.form-style-5 input[type="submit"]:hover,.form-style-5 input[type="button"]:hover{background:#d65050}</style>
    
    </head>
    
    <body>
    <div class="form-style-5">
       <div id="floating-panel">
      <form id="myform">
            <fieldset>
                <legend style="color:black;"><span class="number">1</span> Enter Radius</legend>
                <p id="error"></p>
                <input type="text" id="radius" placeholder="In KM">
                <span id="error"></span>
        
          </fieldset>
     </form>
          <input id="submit" type="submit" value="Search" onclick="return validation();"/>
             
          </div>
      </div>
      
        <script>function validation(){var radius=document.getElementById("radius").value;if(radius==null||radius==""){document.getElementById("error").innerHTML="Enter the radius";return false;}else if(isNaN(radius)){document.getElementById("error").innerHTML="Enter a number";return false;}else{document.getElementById("error").innerHTML="";initMap();return true;}}</script>
        <div id="map" style="width:800px;height:550px;float:right;right: 0px;"></div>  

        
        <script>var temp_lat='';var temp_lng='';var map;var infowindow;getLocation();var im='https://www.aniway.ml/vicon';function getLocation(){if(navigator.geolocation){navigator.geolocation.getCurrentPosition(showPosition);}else{temp_lat=0.0;temp_lng=0.0;}}function showPosition(position){temp_lat=position.coords.latitude;temp_lng=position.coords.longitude;initMap();}function initMap(){var pyrmont={lat:temp_lat,lng:temp_lng};var rd=parseInt(document.getElementById('radius').value)*1000;map=new google.maps.Map(document.getElementById('map'),{center:pyrmont,zoom:12,});infowindow=new google.maps.InfoWindow();var service=new google.maps.places.PlacesService(map);service.nearbySearch({location:pyrmont,radius:rd,type:['veterinary_care']},callback);}function callback(results,status){if(status===google.maps.places.PlacesServiceStatus.OK){for(var i=0;i<results.length;i++){createMarker(results[i]);}}}function createMarker(place){var placeLoc=place.geometry.location;var marker=new google.maps.Marker({map:map,position:place.geometry.location,icon:im});google.maps.event.addListener(marker,'click',function(){infowindow.setContent(place.name+'<br>'+place.vicinity);infowindow.open(map,this);});}</script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBthqk-LcIJvGPTYuOzw4Zxpk7ZadaralQ&amp;libraries=places&amp;callback=initialize" async defer></script>


</body>
</html>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-1f7408b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="1f7408b" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-e3ac020 elementor-column elementor-col-100 elementor-top-column" data-id="e3ac020" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-58efcb4 elementor-widget elementor-widget-spacer" data-id="58efcb4" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
									</div><!-- .entry-content -->

		</main><!-- #main -->
	</div><!-- #primary -->

			</div>
		</div>
	</div><!-- #content -->

	
	
    <a class="go-top"><i class="fa fa-angle-up"></i></a>
		
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<a href="https://wordpress.org/">Copyright @ 2019 ICAI </a>
			<span class="sep">  </span>
			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

	
</div><!-- #page -->

<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/sydney-toolbox/js/main.js?ver=20180228'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/backbone.min.js?ver=1.2.3'></script>
<script type='text/javascript'>//<![CDATA[
var wpApiSettings={"root":"https:\/\/www.aniway.ml\/wp-json\/","nonce":"dd49e7f4e6","versionString":"wp\/v2\/"};
//]]></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/api-request.min.js?ver=5.1.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/wp-api.min.js?ver=5.1.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/js/wp-travel-view-mode.js?ver=1.9.5'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/jquery/ui/slider.min.js?ver=1.11.4'></script>
<script type='text/javascript'>//<![CDATA[
var _wpUtilSettings={"ajax":{"url":"\/wp-admin\/admin-ajax.php"}};
//]]></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/wp-util.min.js?ver=5.1.1'></script>
<script type='text/javascript'>//<![CDATA[
var trip_prices_data={"currency_symbol":"$","prices":"","locale":"en","nonce":"b1d120498d","ajaxUrl":"https:\/\/www.aniway.ml\/wp-admin\/admin-ajax.php"};
//]]></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/js/wp-travel-widgets.js?ver=1.9.5'></script>
<script type='text/javascript'>//<![CDATA[
var wp_travel={"lat":"","lng":"","loc":"","zoom":"15","currency_symbol":"$","prices":"","locale":"en","nonce":"b1d120498d","ajaxUrl":"https:\/\/www.aniway.ml\/wp-admin\/admin-ajax.php","strings":{"confirm":"Are you sure you want to remove?","book_now":"Book Now","book_n_pay":"Book and Pay"},"payment":[]};
//]]></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/js/lib/datepicker/datepicker.js?ver=1.9.5'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/wp-travel/assets/js/lib/datepicker/i18n/datepicker.en.js?ver=1.9.5'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/wp-travel/inc/coupon/assets/js/wp-travel-coupons-frontend.min.js?ver=5.1.1'></script>
<script type='text/javascript'>//<![CDATA[
var wpcf7={"apiSettings":{"root":"https:\/\/www.aniway.ml\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
//]]></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/mystickymenu/js/detectmobilebrowser.js?ver=2.1.1'></script>
<script type='text/javascript'>//<![CDATA[
var option={"mystickyClass":"main-header-bar","activationHeight":"0","disableWidth":"0","disableLargeWidth":"0","adminBar":"false","device_desktop":"1","device_mobile":"1","mystickyTransition":"slide","mysticky_disable_down":"false"};
//]]></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/mystickymenu/js/mystickymenu.min.js?ver=2.1.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/themes/sydney/js/scripts.js?ver=5.1.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/themes/sydney/js/main.min.js?ver=20180716'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/themes/sydney/js/skip-link-focus-fix.js?ver=20130115'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/wp-embed.min.js?ver=5.1.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=2.5.14'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.1'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'></script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=4.4.6'></script>
<script type='text/javascript'>var elementorFrontendConfig={"environmentMode":{"edit":false,"wpPreview":false},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"2.5.14","urls":{"assets":"https:\/\/www.aniway.ml\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_enable_lightbox_in_editor":"yes"}},"post":{"id":1281,"title":"Find Help","excerpt":""}};</script>
<script type='text/javascript' src='https://www.aniway.ml/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.5.14'></script>

</body>
</html>
